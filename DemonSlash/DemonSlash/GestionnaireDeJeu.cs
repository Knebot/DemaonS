﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
    public static class GestionnaireDeJeu
    {
        public static Monster spawnMonster()
        {
            Random rand = new Random();
            int numCreateMonster = rand.Next(1, 4);
            switch (numCreateMonster)
            {
                case 1: return new Gobelin();
                case 2: return new Squelette();
                case 3: return new Wizard();

                default: return new Gobelin();
            }
        }
        public static void specialite(Hero myHero)
        {
            Random rand = new Random();
            int numSpecialite = rand.Next(1, 4);
            switch (numSpecialite)
            {
                case 1:
                    myHero.Life_point += (myHero.Life_point * 20) / 100;
                    myHero.Physical_damage += (myHero.Physical_damage * 10) / 100;
                    Console.WriteLine("\n\n\t-------------Bravo vous etes devenu un valereux Chevalier -------------\n\n\t ");
                    break;

                case 2:
                    myHero.Life_point += (myHero.Life_point *15 )/ 100;
                    myHero.Physical_damage += (myHero.Physical_damage *15)/100;
                    Console.WriteLine("\n\n\t--------------Bravo vous etes  devenu un habil Archer ------------\n\n\t ");
                    break;

                case 3:
                    myHero.Life_point += (myHero.Life_point *10) / 100;
                    myHero.Physical_damage += (myHero.Physical_damage *20 ) /100;
                    Console.WriteLine("\n\n\t------------Bravo vous etes devenu un ruse Magicien-----------\n\n\t  ");
                    break;

            }

        }
        public static void roundFight(Hero hero, Monster monster)
        {
            Console.WriteLine("\n\t-------------------------------\t\n");

            hero.fight(monster);

            if (monster.Life_point <= 0)
            {
                
                    Console.WriteLine("\nLe monstre a été vaincu avec succes !\n");

                    bool resultat = Tresor.potionorTrap(hero);
                    if (resultat)
                    {
                        Console.WriteLine("Vous venez d'avoir une potion felicitation");
                    }
                    else
                    {
                        Console.WriteLine("Vous venez de prendre un piege");
                    }
                    hero.Kill_monsters += 1;
                    monster = spawnMonster();

                    if (hero.Kill_monsters == 5)
                    {
                    Console.WriteLine("VOus venez de ganger une specialisation ");
                        specialite(hero);
                    }
                    roundFight(hero, monster);

            }else
                {
                Console.WriteLine("\n\t-------------------------------\t\n");

                monster.fight(hero);
                    if (hero.Life_point <= 0)
                    {
                        Console.WriteLine("", hero.Kill_monsters);
                        Console.WriteLine("Oh no vous avez perdu dommage");
                    }
                    else
                    {
                        roundFight(hero, monster);
                    }
            } 
        }



    }

}
    
    

