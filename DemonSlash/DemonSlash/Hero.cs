﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
    public  class Hero : Personnage
    {
        private string name;
        private uint kill_monsters;
        public Hero(int life_point, int physical_damage, string name)  
        {
            this.Life_point = life_point;
            this.Physical_damage = physical_damage;
            this.name = name;
            this.kill_monsters = 0;
        }

        public uint Kill_monsters { get => kill_monsters; set => kill_monsters = value; }

        public void TakePotion(Hero hero)
        {
            Random rand = new Random();
           hero.Life_point+= rand.Next(50, 500);
            
        }

    }
}
