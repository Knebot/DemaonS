﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
     public abstract class Personnage
    {
        //attribut de la classe
        private int physical_damage;
        private int life_point;
        //getter setter
        public int Life_point { get => life_point; set => life_point = value; }
        public int Physical_damage { get => physical_damage; set => physical_damage = value; }
        //fontion attaque 
        public void  fight(Personnage personnage1)
        {
           
            personnage1.Life_point -= this.Physical_damage;

    
            
            Console.WriteLine("Cette attaque viens de toucher de plein fouet {0}  il vous reste {1}  hp" ,personnage1.GetType().Name, personnage1.Life_point);
            

        }


    }
}
