﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
    class Squelette : Monster
    {
        public Squelette() 
        {
            Random rand = new Random();
            this.Life_point = rand.Next(300, 500);
            this.Physical_damage = rand.Next(0, 200);
        }
    }
}
