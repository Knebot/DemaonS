﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
    class Tresor
    {
        private static bool suprise;
        public static bool potionorTrap(Hero hero)
        {
            Random rand = new Random();
             int resultrand = rand.Next(0, 2);

            if (resultrand==0)
            {
                suprise = true;
                
                hero.Life_point += rand.Next(50, 250);
                Console.WriteLine("Vous venez de gagner {0}", hero.Life_point);
            }
            else
            {
                suprise = false;
                hero.Life_point -= rand.Next(50, 250);
                Console.WriteLine("Vous venez de perdre {0}", hero.Life_point);
            }
            return suprise;
        }
    }
}
