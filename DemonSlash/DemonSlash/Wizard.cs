﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonSlash
{
    class Wizard : Monster
    {
        public Wizard() 
        {
            Random rand = new Random();
            this.Life_point = rand.Next(100, 350);
            this.Physical_damage = rand.Next(200, 500);
        }
    }
}
